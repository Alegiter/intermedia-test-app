import {SafeHtml} from "@angular/platform-browser";

export interface AppIcon {
  name:string,
  width:number,
  svg:SafeHtml
}
