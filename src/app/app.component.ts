import {Component} from '@angular/core';
import {BASIC_ICONS} from "./test/basic_icons";
import {DomSanitizer, SafeHtml} from "@angular/platform-browser";
import {AppIcon} from "./_source/app-icon";
import {Observable, of} from "rxjs";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent {

  private icons: AppIcon[];
  private iconWidths:number[];

  private filteredIcons:Observable<AppIcon[]>;

  private coloringButtons:string[] = ['Blue', 'Green', 'Red', 'Orange'];

  constructor(private sanitizer:DomSanitizer) {
    this.prepareIcons();
  }

  private prepareIcons() {

    const numRegExp = (val:string) => {
      if (!val) return null;
      return Number(val.replace(/\D/g, ""))
    };

    this.icons = Object.keys(BASIC_ICONS)
      .map(key => {
        const parser = new DOMParser();
        const svgEl:Element = parser.parseFromString(BASIC_ICONS[key], "image/svg+xml").querySelector('svg');
        const svg:SafeHtml = this.sanitizer.bypassSecurityTrustHtml(svgEl.outerHTML);
        const svgElWidth:number = numRegExp(svgEl.getAttribute('width'));

        return <AppIcon>{name:key, width:svgElWidth, svg:svg}
      })
      .sort((a:AppIcon, b:AppIcon) => {
        return a.name.localeCompare(b.name)
      })
      .sort((a:AppIcon, b:AppIcon) => {
        if (!a.width) return 1;
        if (!b.width) return -1;
        if (a.width < b.width) return -1;
        if (a.width > b.width) return 1;
        return 0;
      });

    this.prepareIconWidths();
    this.filterIcons()
  }

  private prepareIconWidths() {
    this.iconWidths = this.icons.map((ic:AppIcon) => ic.width).filter((item, i, arr) => arr.indexOf(item) === i);
  }

  private filterIcons(value?:string) {
    if (!value || value == '') {
      this.filteredIcons = of(this.icons);
    }
    else this.filteredIcons = of(this.icons.filter((ic:AppIcon) => ic.name.match(value)));
  }

  private changeIconsColor(button:Element) {
    const color = window.getComputedStyle(button).getPropertyValue('background-color');

    Array.from(document.getElementsByTagName("svg"))
      .forEach(el => {
        Array.from(el.children).forEach(child => child.setAttribute('style', `fill:${color}!important`))
      })
  }

  private changeIconsColorToDefault() {
    Array.from(document.getElementsByTagName("svg"))
      .forEach(el => {
        Array.from(el.children).forEach(child => child.removeAttribute('style'))
      })
  }


}
