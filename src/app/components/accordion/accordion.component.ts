import {Component, Input} from '@angular/core';
import {BASIC_ICONS} from "../../test/basic_icons";
import {DomSanitizer, SafeHtml} from "@angular/platform-browser";

@Component({
  selector: 'app-accordion',
  templateUrl: './accordion.component.html',
  styleUrls: ['./accordion.component.less']
})
export class AccordionComponent {

  @Input() title:string;

  private state: "expanded" | "collapsed" = "collapsed";

  private iconExpand:SafeHtml;
  private iconCollapse:SafeHtml;

  constructor(private sanitizer:DomSanitizer) {
    this.iconCollapse = this.sanitizer.bypassSecurityTrustHtml(BASIC_ICONS["chevron-up-16"]);
    this.iconExpand = this.sanitizer.bypassSecurityTrustHtml(BASIC_ICONS["chevron-down-16"]);
  }

  toggle() {
    if (this.state == "expanded") this.state = "collapsed";
    else this.state = "expanded";
  }
}
